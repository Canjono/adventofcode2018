﻿using System;
using adventOfCode.Solutions;

namespace adventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
//            var day1 = new Day1();
//            day1.StartFirst();
//            day1.StartSecond();

//            var day2 = new Day2();
//            day2.StartFirst();
//            day2.StartSecond();

//            var day3 = new Day3();
//            day3.StartFirst();
//            day3.StartSecond();

//            var day4 = new Day4();
//            day4.StartFirst();
//            day4.StartSecond();

            var day5 = new Day5();
            day5.StartFirst();
            day5.StartSecond();
        }
    }
}
