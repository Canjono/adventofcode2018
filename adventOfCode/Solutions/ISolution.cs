namespace adventOfCode.Solutions
{
    public interface ISolution
    {
         void StartFirst();
         void StartSecond();
    }
}