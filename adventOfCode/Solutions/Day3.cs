using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace adventOfCode.Solutions
{
    public class Day3 : ISolution
    {
        private int[,] _grid;
        private readonly List<FabricClaim> _claims = new List<FabricClaim>();
        private static string ResourcePath => Directory.GetCurrentDirectory() + "/Resources/Day3.txt";

        public void StartFirst()
        {
            var maxWidth = 1000;
            var maxHeight = 1000;

            // Ex: #1 @ 1,3: 4x4
            var regex = new Regex(@"#(\d*) @ (\d*),(\d*): (\d*)x(\d*)");

            foreach (var line in File.ReadLines(ResourcePath))
            {
                var claim = GetFabricClaim(line, regex);
                _claims.Add(claim);

                SetMaxGridSize(ref maxWidth, ref maxHeight, claim);
            }

            _grid = new int[maxWidth, maxHeight];

            FillGrid();

            var amountOfOverlapping = GetAmountOfOverlapping();

            Console.WriteLine($"Day 3-1 result: {amountOfOverlapping}");
        }

        private static FabricClaim GetFabricClaim(string line, Regex regex)
        {
            var groups = regex.Matches(line)[0].Groups;

            return new FabricClaim(groups[1].Value, groups[2].Value, groups[3].Value, groups[4].Value, groups[5].Value);
        }

        private static void SetMaxGridSize(ref int maxWidth, ref int maxHeight, FabricClaim claim)
        {
            var rightCoordinate = claim.LeftOffset + claim.Width;

            if (rightCoordinate > maxWidth)
            {
                maxWidth = rightCoordinate;
            }

            var bottomCoordinate = claim.TopOffset + claim.Height;

            if (bottomCoordinate > maxHeight)
            {
                maxHeight = bottomCoordinate;
            }
        }

        private void FillGrid()
        {
            foreach (var claim in _claims)
            {
                for (var i = claim.LeftOffset; i < claim.LeftOffset + claim.Width; i++)
                {
                    for (var j = claim.TopOffset; j < claim.TopOffset + claim.Height; j++)
                    {
                        _grid[i, j]++;
                    }
                }
            }
        }

        private int GetAmountOfOverlapping()
        {
            var amountOfOverlapping = 0;

            for (var i = 0; i < _grid.GetLength(0); i++)
            {
                for (var j = 0; j < _grid.GetLength(1); j++)
                {
                    if (_grid[i, j] > 1)
                    {
                        amountOfOverlapping++;
                    }
                }
            }

            return amountOfOverlapping;
        }

        public void StartSecond()
        {
            if (_grid == null)
            {
                StartFirst();
            }

            foreach (var claim in _claims)
            {
                var hasOverlaps = CheckIfHasOverlaps(claim);

                if (!hasOverlaps)
                {
                    Console.WriteLine($"Day 3-2 result: {claim.Id}");
                    break;
                }
            }
        }

        private bool CheckIfHasOverlaps(FabricClaim claim)
        {
            for (var i = claim.LeftOffset; i < claim.LeftOffset + claim.Width; i++)
            {
                for (var j = claim.TopOffset; j < claim.TopOffset + claim.Height; j++)
                {
                    if (_grid[i, j] != 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    internal struct FabricClaim
    {
        public readonly int Id;
        public readonly int LeftOffset;
        public readonly int TopOffset;
        public readonly int Width;
        public readonly int Height;


        public FabricClaim(string id, string leftOffset, string topOffset, string width, string height)
        {
            Id = int.Parse(id);
            LeftOffset = int.Parse(leftOffset);
            TopOffset = int.Parse(topOffset);
            Width = int.Parse(width);
            Height = int.Parse(height);
        }
    }
}