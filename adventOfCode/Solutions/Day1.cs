using System;
using System.Collections.Generic;
using System.IO;

namespace adventOfCode.Solutions
{
    public class Day1 : ISolution
    {
        private int _frequency;
        private readonly List<int> _frequencyChanges = new List<int>();
        private readonly HashSet<int> _uniqueFrequencies = new HashSet<int>();
        private static string ResourcePath => Directory.GetCurrentDirectory() + "/Resources/Day1.txt";

        private void Init()
        {
            _frequency = 0;
            SetFrequencyChanges();
            _uniqueFrequencies.Clear();
        }

        public void StartFirst()
        {
            Init();

            _frequencyChanges.ForEach(x => _frequency += x);

            Console.WriteLine($"Day 1-1 result: {_frequency}");
        }

        public void StartSecond()
        {
            Init();

            _uniqueFrequencies.Add(_frequency);

            while (!FindDuplicateFrequency()) {}

            Console.WriteLine($"Day 1-2 result: {_frequency}");
        }

        private void SetFrequencyChanges()
        {
            _frequencyChanges.Clear();

            foreach (var line in File.ReadLines(ResourcePath))
            {
                if (int.TryParse(line, out var number))
                {
                    _frequencyChanges.Add(number);
                }
            }
        }

        private bool FindDuplicateFrequency()
        {
            foreach (var change in _frequencyChanges)
            {
                _frequency += change;

                if (!_uniqueFrequencies.Add(_frequency))
                {
                    return true;
                }
            }

            return false;
        }
    }
}