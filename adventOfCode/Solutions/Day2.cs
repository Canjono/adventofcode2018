using System;
using System.IO;
using System.Linq;

namespace adventOfCode.Solutions
{
    public class Day2 : ISolution
    {
        private static string ResourcePath => Directory.GetCurrentDirectory() + "/Resources/Day2.txt";

        public void StartFirst()
        {
            var twoLetters = 0;
            var threeLetters = 0;

            foreach (var line in File.ReadLines(ResourcePath))
            {
                var twosAndThrees = FindTwosAndThrees(line);

                if (twosAndThrees.Item1)
                {
                    twoLetters++;
                }

                if (twosAndThrees.Item2)
                {
                    threeLetters++;
                }
            }

            Console.WriteLine($"Day 2-1 result: {twoLetters * threeLetters}");
        }

        private static Tuple<bool, bool> FindTwosAndThrees(string line)
        {
            var groupedLetters = line.GroupBy(x => x);
            var foundTwos = groupedLetters.Any(x => x.Count() == 2);
            var foundThrees = groupedLetters.Any(x => x.Count() == 3);

            return new Tuple<bool, bool>(foundTwos, foundThrees);
        }

        public void StartSecond()
        {
            var lines = File.ReadLines(ResourcePath).ToArray();

            for (var i = 0; i < lines.Length - 1; i++)
            {
                var line = lines[i];

                for (var j = i + 1; j < lines.Length; j++)
                {
                    var otherLine = lines[j];

                    if (FindMatchingLines(line, otherLine))
                    {
                        var commonLetters = GetCommonLetters(line, otherLine);

                        Console.Write($"Day 2-2 result: {commonLetters}");
                        return;
                    }
                }
            }
        }

        private static bool FindMatchingLines(string line1, string line2)
        {
            var differentChars = 0;

            for (var i = 0; i < line1.Length; i++)
            {
                if (line1[i] != line2[i] && ++differentChars == 2)
                {
                    return false;
                }
            }

            return true;
        }

        private static string GetCommonLetters(string line1, string line2)
        {
            var commonLetters = line1.Where((letter, i) => letter == line2[i]);

            return string.Join("", commonLetters);
        }
    }
}