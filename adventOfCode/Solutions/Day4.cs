using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace adventOfCode.Solutions
{
    public class Day4 : ISolution
    {
        private static string ResourcePath => Directory.GetCurrentDirectory() + "/Resources/Day4.txt";

        public void StartFirst()
        {
            var sortedLines = GetSortedLines();
            var guards = InitGuards(sortedLines);

            (int Id, int TotalSleepTime, List<(int Sleep, int Awake)> SleepRanges) sleepiestGuard =
                (-1, -1, new List<(int sleep, int awake)>());

            foreach (var guard in guards)
            {
                if (guard.Value.TotalSleepTime > sleepiestGuard.TotalSleepTime)
                {
                    sleepiestGuard = (guard.Key, guard.Value.TotalSleepTime, guard.Value.SleepRanges);
                }
            }

            var minuteSleptMost = GetMinuteSleptMost(sleepiestGuard.SleepRanges).Minute;

            Console.WriteLine($"Day 4-1 result: {sleepiestGuard.Id * minuteSleptMost}");
        }

        private static List<(DateTime Date, string Text)> GetSortedLines()
        {
            var sortedLines = new List<(DateTime Date, string Text)>();
            
            // Ex: [1518-10-29 00:36] falls asleep
            var dateRegex = new Regex(@"(\d{4}-\d{2}-\d{2} \d{2}:\d{2})] (.*)");

            foreach (var line in File.ReadLines(ResourcePath))
            {
                var groups = dateRegex.Matches(line)[0].Groups;
                var date = DateTime.Parse(groups[1].Value);
                var text = groups[2].Value;

                sortedLines.Add((date, text));
            }

            sortedLines.Sort((a, b) => a.Date.CompareTo(b.Date));

            return sortedLines;
        }

        private static Dictionary<int, SleepData> InitGuards(List<(DateTime Date, string Text)> lines)
        {
            var guards = new Dictionary<int, SleepData>();
            var idRegex = new Regex(@"#(\d*)");
            var wentToSleep = 0;
            var currentGuardId = 0;

            for (var i = 0; i < lines.Count; i++)
            {
                var text = lines[i].Text;
                var currentMinute = lines[i].Date.Minute;
                var startingLetter = text[0];

                switch (startingLetter)
                {
                    case 'G':
                        var id = idRegex.Matches(text)[0].Groups[1].Value;
                        int.TryParse(id, out currentGuardId);

                        if (!guards.ContainsKey(currentGuardId))
                        {
                            guards.Add(currentGuardId, new SleepData());
                        }

                        break;
                    case 'f':
                        wentToSleep = currentMinute;
                        break;
                    case 'w':
                        guards[currentGuardId].TotalSleepTime += currentMinute - wentToSleep;
                        guards[currentGuardId].SleepRanges.Add((wentToSleep, currentMinute));
                        break;
                }
            }

            return guards;
        }

        private static (int Minute, int Times) GetMinuteSleptMost(List<(int Sleep, int Awake)> sleepRanges)
        {
            var minuteTimes = new Dictionary<int, int>();
            (int Minute, int Times) minuteAndTimes = (-1, -1);

            foreach (var range in sleepRanges)
            {
                for (var i = range.Sleep; i < range.Awake; i++)
                {
                    if (minuteTimes.ContainsKey(i))
                    {
                        if (++minuteTimes[i] > minuteAndTimes.Times)
                        {
                            minuteAndTimes.Times = minuteTimes[i];
                            minuteAndTimes.Minute = i;
                        }
                    }
                    else
                    {
                        minuteTimes.Add(i, 1);
                    }
                }
            }

            return minuteAndTimes;
        }

        public void StartSecond()
        {
            var sortedLines = GetSortedLines();
            var guards = InitGuards(sortedLines);

            (int id, int minute, int times) guardWithLongestMinute = (-1, -1, -1);

            foreach (var guard in guards)
            {
                var minuteAndTimes = GetMinuteSleptMost(guard.Value.SleepRanges);

                if (minuteAndTimes.Times > guardWithLongestMinute.times)
                {
                    guardWithLongestMinute = (guard.Key, minuteAndTimes.Minute, minuteAndTimes.Times);
                }
            }

            Console.WriteLine($"Day 4-2 result: {guardWithLongestMinute.id * guardWithLongestMinute.minute}");
        }

        private class SleepData
        {
            public int TotalSleepTime = 0;
            public readonly List<(int Sleep, int Awake)> SleepRanges = new List<(int sleep, int awake)>();
        }
    }
}