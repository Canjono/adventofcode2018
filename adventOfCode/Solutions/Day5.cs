using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventOfCode.Solutions
{
    public class Day5 : ISolution
    {
        private static string ResourcePath => Directory.GetCurrentDirectory() + "/Resources/Day5.txt";
        
        public void StartFirst()
        {
            var polymer = File.ReadAllText(ResourcePath);

            for (var i = 0; i < polymer.Length - 1; i++)
            {
                var current = polymer[i];
                var next = polymer[i + 1];

                if (AreSameType(current, next) && HaveDifferentPolarity(current, next))
                {
                    polymer = polymer.Remove(i, 2);
                    i = Math.Max(-1, i - 2);
                }
            }

            Console.WriteLine($"Day 5-1 result: {polymer.Length}");
        }

        private static bool AreSameType(char unit, char otherUnit)
        {
            return char.ToLowerInvariant(unit) == char.ToLowerInvariant(otherUnit);
        }

        private static bool HaveDifferentPolarity(char unit, char otherUnit)
        {
            return (char.IsUpper(unit) && char.IsLower(otherUnit)) ||
                   (char.IsLower(unit) && char.IsUpper(otherUnit));
        }

        public void StartSecond()
        {
            var unitLengths = new Dictionary<char, int>();
            var alphabet = new char[26];
            var letterChar = 'a';
            
            for (var i = 0; i < 26; i++)
            {
                alphabet[i] = letterChar++;
            }

            foreach (var letter in alphabet)
            {
                var polymer = File.ReadAllText(ResourcePath);
                
                for (var i = 0; i < polymer.Length - 1; i++)
                {
                    var current = polymer[i];
                    var next = polymer[i + 1];
                    
                    if (AreSameType(current, next) && HaveDifferentPolarity(current, next))
                    {
                        polymer = polymer.Remove(i, 2);
                        i = Math.Max(-1, i - 2);
                    }
                    else if (AreSameType(letter, current))
                    {
                        polymer = polymer.Remove(i, 1);
                        i = Math.Max(-1, i - 2);
                    }
                }
                
                unitLengths.Add(letter, polymer.Length);
            }

            var shortestPolymerLength = unitLengths.Values.Aggregate(50000, Math.Min);
            
            Console.WriteLine($"Day 5-2 result: {shortestPolymerLength}");
        }
    }
}